import { booster } from '@booster-ts/core';
import { injector } from '../../injector';
import express = require("express");
import { Express, json } from 'express';
import bodyParser = require('body-parser');
import { PrometheusModule } from '../Prometheus/Prometheus';
const cors = require('cors');

/**
 * ExpressModule
 * @description Express Wrapper
 * @author ImOverlord
 */
@booster()
export class ExpressModule {

    /** Express App */
    private app: Express;
    /** Api Router */
    private router: express.Router = express.Router();

    constructor(
        private prometheus: PrometheusModule
    ) {
        this.app = express();
    }

    /**
     * init
     * @description init Express app
     */
    public init(): Promise<void> {
        return new Promise((resolve, reject) => {
            const PORT = 3000;

            this.app.listen(PORT, () => {
                this.router.use(json());
                this.router.use(this.prometheus.getMiddleware());
                this.router.use(cors());
                this.app.get('/metrics', this.prometheus.getMetrics);
                this.app.use('/api/v1/', this.router);
                console.log(`App Started on http://localhost:${PORT}`);
                resolve();
            })
            .once('error', (error) => {
                reject(error);
            });
        });
    }

    /**
     * getApp
     * @description Returns Express app
     */
    public getApp(): express.Router {
        return this.router;
    }
}

injector.register("ExpressModule", ExpressModule);
