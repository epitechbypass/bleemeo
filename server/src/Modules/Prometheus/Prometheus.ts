import { booster } from '@booster-ts/core';
import { injector } from '../../injector';
import express = require("express");
import { Counter, Histogram } from 'prom-client';
import { register } from 'prom-client';
import { Request, Response } from 'express';

/**
 * ExpressModule
 * @description Express Wrapper
 */
@booster()
export class PrometheusModule {

    private counter = new Counter({
        name: "node_request_operations_total",
        help: "Total Number of request"
    });

    private successResponseCode = new Counter({
        name: 'node_request_success_response_type',
        help: "Histogram of 2xx Response Code",
    });

    private clientErrorResponseCode = new Counter({
        name: 'node_request_client_error_response_type',
        help: "Histogram of 4xx Response Code",
    });

    private serverErrorResponseCode = new Counter({
        name: 'node_request_server_error_response_type',
        help: "Histogram of 5xx Response Code",
    });

    private histogram = new Histogram({
        name: "node_request_duration_seconds_count",
        help: "Histogram of duration",
        buckets: [1, 2, 5, 6, 10]
    });

    private averageUsersAdded = new Histogram({
        name: "node_request_average_users_added",
        help: "Average Users Added By Session",
        buckets: [1]
    });

    private randomNumberGenerated = new Histogram({
        name: "node_request_random_number",
        help: "Random Number Generated",
        buckets: [1]
    })

    public getMiddleware(): any {
        return (req: express.Request, res: express.Response, next: Function) => {
            if (req.path === '/metrics')
                return next();
            const timestamp = Date.now();
            req.on('data', () => {});
            req.on('end', () => {
                console.log("End Reponse");
                if (res.statusCode >= 200 && res.statusCode < 300)
                    this.successResponseCode.inc();
                if (res.statusCode >= 400 && res.statusCode < 500)
                    this.clientErrorResponseCode.inc();
                if (res.statusCode >= 500 && res.statusCode < 600)
                    this.serverErrorResponseCode.inc();
                const duration = Date.now() - timestamp;
                this.histogram.observe(duration / 1000);
            });
            this.counter.inc();
            next();
        }
    }

    public addAverageUsers(count: number): void {
        this.averageUsersAdded.observe(count);
    }

    public addRandomNumber(count: number): void {
        this.randomNumberGenerated.observe(count);
    }

    public getMetrics(req: Request, res: Response) {
        res.set('Content-Type', register.contentType);
        res.send(register.metrics());
    }

}

injector.register("PrometheusModule", PrometheusModule);
