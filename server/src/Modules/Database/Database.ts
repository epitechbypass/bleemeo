import { booster } from '@booster-ts/core';
import { injector } from '../../injector';
import { SimpleSql } from '@everydayheroes/simplesql';
import { Client, QueryResult } from 'pg';


/**
 * DatabaseModule
 * @description Newly Generated Module
 * @author Booster-cli
 */
@booster({
    type: "Module"
})
export class DatabaseModule {

    /** PG Client */
    private client: Client;
    /** SimpleSQL Engine */
    private engine: SimpleSql;

    constructor() {
        this.engine = new SimpleSql('pgEngine');
        this.client = new Client({
            connectionString: process.env.DATABASE_URL_PLATFORM
        });
    }

    /**
     * Init
     * @description Init Database Connection
     */
    public init(): Promise<any> {
        return this.client.connect()
        .then(() => {
            return this.client.query("SET SCHEMA 'test'");
        });
    }

    /**
     * query
     * @description Query Database
     * @param command to send
     * @param args in command
     * @param T type of the row
     */
    public query<T extends object = any>(command: string, args?: Array<any>): Promise<QueryResult<T>> {
        if (args === undefined)
            args = [];
        return new Promise<QueryResult<T>>((resolve, reject) => {
            this.client.query(command, args)
            .then((result: QueryResult<T>) => {
                resolve(result);
            })
            .catch((error) => {
                reject({
                    code: "99",
                    text: "Server Error",
                    httpResponse: 500,
                    serverError: error
                });
            });
        });
    }

    /**
     * query
     * @description Query Database but will never fail
     * @param command to send
     * @param args in command
     * @param T type of the row
     */
    public queryNoFail(command: string, args?: Array<any>): Promise<undefined> {
        if (args === undefined)
            args = [];
        return new Promise<undefined>((resolve, reject) => {
            this.client.query(command, args)
            .then(() => {
                resolve(undefined);
            })
            .catch(() => {
                resolve(undefined);
            });
        });
    }

    /**
     * select
     * @param tableName to select
     * @param data filter
     */
    public select<T extends object = any>(tableName: string, data: object): Promise<QueryResult<T>> {
        const req = this.engine.select(tableName, data);
        return this.query<T>(req.query, req.data);
    }

    /**
     * insert
     * @description Add a new Row
     * @param tableName where to insert
     * @param data to insert
     */
    public insert<T extends object = any>(tableName: string, data: T): Promise<QueryResult<T>> {
        const request = this.engine.insert(tableName, data);
        return this.query<T>(`${request.query} RETURNING *;`, request.data);
    }

    /**
     * update
     * @description Update fields
     * @param tableName where to update
     * @param infoToUpdate what should change
     * @param filter of which row to modify
     */
    public update<T extends object = any>(tableName: string, infoToUpdate: object, filter: object): Promise<QueryResult<T>> {
        const request = this.engine.update(tableName, infoToUpdate, filter);
        return this.query<T>(request.query, request.data);
    }

    /**
     * delete
     * @description Remove rows from table
     * @param tableName where to delte
     * @param filter of which row to delete
     */
    public delete<T extends object = any>(tableName: string, filter: object): Promise<QueryResult<T>> {
        const request = this.engine.delete(tableName, filter);
        return this.query<T>(request.query, request.data);
    }
}

injector.register("DatabaseModule", DatabaseModule);
