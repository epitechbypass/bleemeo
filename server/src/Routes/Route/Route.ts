import { booster } from '@booster-ts/core';
import express = require("express");
import { Request, Response} from 'express';
import { ExpressModule } from '../../Modules/Express/Express';
import { PrometheusModule } from '../../Modules/Prometheus/Prometheus';

/**
 * ExpressModule
 * @description Express Wrapper
 */
@booster()
export class Route {

    private app: express.Router;
    private names: Array<string> = [];

    constructor(
        private express: ExpressModule,
        private prom: PrometheusModule
    ) {
        this.app = express.getApp();
        this.app.get('/', this.home.bind(this));
        this.app.get('/users', this.getUsers.bind(this));
        this.app.post('/add', this.add.bind(this));
        this.app.post('/remove', this.remove.bind(this));
        this.app.get('/distribution', this.distribution.bind(this));
    }

    private home(req: Request, res: Response): void {
        res.status(200).json({
            code: "00",
            text: "OK"
        });
    }

    private getUsers(req: Request, res: Response): void {
        res.status(200).send({
            code: "00",
            text: "Returning Users",
            data: {
                users: this.names
            }
        });
    }

    private add(req: Request, res: Response): void {
        const namesInput = req.body.names;

        if (!namesInput || !Array.isArray(namesInput)) {
            res.status(400).json({
                code: "01",
                text: "KO"
            });
        } else {
            this.names = namesInput;
            this.prom.addAverageUsers(namesInput.length)
            res.status(200).json({
                code: "00",
                text: "OK"
            });
        }
    }

    private remove(req: Request, res: Response): void {
        const nameInput = req.body.name;

        if (!nameInput) {
            res.status(400).json({
                code: "01",
                text: "KO"
            });
        } else {
            const index = this.names.indexOf(nameInput);

            if (index >= 0) {
                this.names.splice(index, 1);
                res.status(200).json({
                    code: "00",
                    text: "OK"
                });
            } else {
                res.status(400).json({
                    code: "01",
                    text: "KO"
                });
            }
        }
    }

    private distribution(req: Request, res: Response): void {
        const result = {};
        const numbers = [];

        if (this.names.length === 0) {
            res.status(500).json({
                code: "01",
                text: "No Users"
            });
            return;
        }
        for (const name of this.names) {
            let nbr = Math.floor(Math.random() * Math.floor(this.names.length));

            while (numbers.indexOf(nbr) !== -1)
                nbr = Math.floor(Math.random() * Math.floor(this.names.length));
            this.prom.addRandomNumber(nbr);
            numbers.push(nbr);
            result[name] = nbr;
        }
        res.status(200).json({
            code: "00",
            text: "OK",
            data: result
        });
    }

}
