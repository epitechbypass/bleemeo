import { injector } from "./injector";
import { ExpressModule } from "./Modules/Express/Express";
import { Route } from './Routes/Route/Route';
import { DatabaseModule } from "./Modules/Database/Database";

Promise.all([
    injector.inject(ExpressModule).init(),
    injector.inject(DatabaseModule).init()
])
.then(() => {
    injector.inject(Route);
    console.log("App Started");
});
