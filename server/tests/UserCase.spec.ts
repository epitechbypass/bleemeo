import request = require("superagent");
import { BASE_URL } from "./env";

describe('Base Test', () => {

    const names = ['John', 'Annie'];

    it("Should add Users", (done) => {
        request.post(`${BASE_URL}/add`)
        .send({
            names
        }).then((res) => {
            expect(res.status).toBe(200);
            expect(res.body.code).toBe("00");
            return request.get(`${BASE_URL}/users`);
        })
        .then((res) => {
            expect(res.status).toBe(200);
            expect(res.body.code).toBe("00");
            expect(res.body.data.users).toStrictEqual(names);
            done();
        })
        .catch((error) => {
            done(error);
        })
    })

    it("Should fail on non Array", (done) => {
        request.post(`${BASE_URL}/add`)
        .send({
            names: "test"
        })
        .end((err, res) => {
            expect(res.status).toBe(400);
        })
    })
})
